#!/bin/sh -e
#
# alert-ssl-cert-expiration
#
# Copyright (C) 2002-2014 Simó Albert i Beltran
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

WINDOW_ALERT=${WINDOW_ALERT:-1week}
DEBUG=${DEBUG:-false}

if [ $# -lt 1 ]
then
	echo "Usage: $0 <cert_file> [another_cert_file]..." >&2
	exit 1
fi

for cert_file in $*
do
	if [ ! -r "$cert_file" ]
	then
		echo "Error: You don't have read permissions for $cert_file" >&2
		continue
	fi

	expiration_date_string="$(openssl x509 -in $cert_file -enddate -noout | sed s/notAfter=//)"

	if [ -z "$expiration_date_string" ]
	then
		echo "Error: expiration date not obtained for $cert_file" >&2
		continue
	fi

	# Ignore year 2038 problem
	expiration_year=$(echo $expiration_date_string | cut -d" " -f4)
	if [ $expiration_year -ge 2038 ]
	then
		echo "Error: ignoring date greater than 2038 for $cert_file" >&2
		continue
	fi

	expiration_date="$(date "+%s" -d "$expiration_date_string")"

	if [ "$expiration_date" -lt "$(date "+%s" -d "+$WINDOW_ALERT")" ]
	then
		echo "ALERT! Less than $WINDOW_ALERT to expire $cert_file ssl certificate, it expires on $(date -d "@$expiration_date")"
	elif $DEBUG
	then
		echo "$cert_file ssl certificate expire on $(date -d "@$expiration_date")"
	fi
done
